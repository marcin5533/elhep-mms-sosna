/**
 * 
 */
package com.example.elhep_mms;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;

import android.os.Handler;
import android.util.Log;

/**
 * @author Agnieszka Zago�dzi�ska, Andrzej Woje�ski
 *
 */
// Manages connections after sending DISCOVERY packets
// SSDP server
public class ConnectionServer extends Thread { //implements Runnable  {
	
	private Handler rh_;
	private boolean run = true;
	private DatagramSocket socket;
	private boolean flag_ssdp = false;
	private Map<String, Websocket_connection> connections_map;
	
	public final static String DISCOVERY_PACKET = "M-SEARCH * HTTP/1.1\n" +
			"HOST: 239.255.255.250:1900\n" +			
			"MAN: \"ssdp:discover\"\n" +
			"MX: 5.0\n" +
			"ST:urn:ELHEP:device:MMS:";
	
	ConnectionServer (Handler h) {
		rh_ = h; 
		connections_map = new HashMap<String, Websocket_connection>();
	}
	
	public void closeWebsocket(Device_data data) {
		
		Websocket_connection ws_conn = connections_map.get(data.IP);
		
		if (ws_conn != null) {
			Log.d(MainActivity.DEBUG_SERVER, "Connection closed with IP: " + data.IP);
			ws_conn.closeConn();
			
			// remove from list
			connections_map.remove(data.IP);
		}		
		
	}
	
	public void sendDataToDevice(Device_data data) {
		
		Websocket_connection ws_conn = connections_map.get(data.IP);
		
		if (ws_conn != null) {
			Log.d(MainActivity.DEBUG_SERVER, "Data sent to IP: " + data.IP);
			ws_conn.sendData(data);
		}
	}
	
	public void send_ssdp_search() throws IOException {
		
		// Sending discovery packet
		Log.d(MainActivity.DEBUG_SERVER, "SENDING SSDP DISCOVERY PACKAGE");

		InetAddress udp_broadcast = InetAddress.getByName("239.255.255.250");
		//InetAddress udp_broadcast = InetAddress.getByName("192.168.0.255");

		byte[] msg_ssdp = DISCOVERY_PACKET.getBytes();
		DatagramPacket packet_ssdp = new DatagramPacket(msg_ssdp, msg_ssdp.length, udp_broadcast, MainActivity.CONNECTION_PORT);
		socket.send(packet_ssdp);
		
	}
	
	public void ssdp_search() {
				
		// close all connections through Websockets		
		for (Map.Entry<String, Websocket_connection>entry : connections_map.entrySet()) {
			entry.getValue().closeConn();	
			/*	those lines should be implemented... (WebsocketConnector not closing, so thread also...)
			try {
				entry.getValue().join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			*/
			
		}
		
		connections_map.clear();
		
		flag_ssdp = true;
	}
	
	public void stop_server() { // stop thread and close socket
		run = false;
	}
	
	private void closeConn() {
		
		// close ssdp socket
		if (socket != null)
			socket.close();
		
		// close all websockets
		for (Map.Entry<String, Websocket_connection>entry : connections_map.entrySet()) {
			entry.getValue().closeConn();	
			// those lines should be implemented... (WebsocketConnector not closing, so thread also...)
			//try {
			//	entry.getValue().join();
			//} catch (InterruptedException e) {
			//	// TODO Auto-generated catch block
			//	e.printStackTrace();
			//}
		}		
		
	}
	
	public void run() {		
		
		byte[] buf = new byte[1024];
		
		try {	
			
			socket = new DatagramSocket(MainActivity.CONNECTION_PORT);	
			socket.setBroadcast(true);
			socket.setSoTimeout(1000);
			
			// set timeout
			Log.d(MainActivity.DEBUG_SERVER, "WAITING FOR DATA");
			
			while (run) {
				
				if (flag_ssdp) {
					flag_ssdp = false;
					send_ssdp_search();
				}
			
				//Log.d(MainActivity.DEBUG_SERVER, "WAITING FOR DATA");
				// Prints discovery packets on screen				
				DatagramPacket packet = new DatagramPacket(buf, buf.length);
				
				try {
					socket.receive(packet);
				} catch (InterruptedIOException e) {
					continue;
				}
			
				String packet_data = new String(packet.getData(), 0, packet.getLength());
			
				Log.d(MainActivity.DEBUG_SERVER, "SERVER: " + packet_data.length() + ", PACKET DATA: " + packet_data);
				Log.d(MainActivity.DEBUG_SERVER, "LENGTH: " + packet.getLength());
				Log.d(MainActivity.DEBUG_SERVER, "SENDER IP: " + packet.getAddress().getHostAddress());
				
				// discard broadcast discovery message
				// Discard data if its send from localhost
				// 1 - test only				
				if (DISCOVERY_PACKET.equals(packet_data)) { 
					Log.d(MainActivity.DEBUG_SERVER, "BROADCAST_DISCOVERY");
					return;
				}
				else {	
					
					// check if data is from MMS system
					//if (!packet_data.contains("urn:ELHEP:device:MMS:0.1"))
					//	continue;
					
					Log.d(MainActivity.DEBUG_SERVER, "NEW_MEASUREMENT_DEVICE");
					
					Websocket_connection conn = new Websocket_connection(packet.getAddress().getHostAddress());					
					// create map <IP, Websocket handler>
					if (connections_map.get(packet.getAddress().getHostAddress()) == null) {
						connections_map.put(packet.getAddress().getHostAddress(), conn);
						// open Websocket connection
						conn.start();				
					}
					else
						Log.d(MainActivity.DEBUG_SERVER, "NEW_MEASUREMENT_DEVICE: Already on list");
					
					// Create new data pack for measurement device
					//Device_data device_data_i = new Device_data();
					// Assign IP
					//device_data_i.IP = packet.getAddress().getHostAddress();
					
					// Start new connection through Websockets with device
					
					// Add data packet do vector for management
					
					// register device in vector database (TODO)
					
					//Message msg = rh_.obtainMessage();			
					//Bundle b = new Bundle();	
					
					//b.putString("PACKET_DATA", packet_data);
					//b.putInt("PACKET_LENGTH", packet.getLength());
					//b.putString("SENDER_IP", packet.getAddress().getHostAddress());			
				
					//msg.setData(b);
					//rh_.sendMessage(msg);				
				
				}
				
		   }
						
			// thread ended
			closeConn();
			
		} catch (SocketException e) {
			
			closeConn();
			
			Log.d(MainActivity.DEBUG_SERVER, "SOCKET_EXCEPTION"); 
			
			// print data to user (can't open socket)
				
			//e.printStackTrace();
				
		} catch (IOException e) {
				
			closeConn();
				
			//e.printStackTrace();
			Log.d(MainActivity.DEBUG_SERVER, "IO_EXCEPTION");
		}			
		
	}
}

