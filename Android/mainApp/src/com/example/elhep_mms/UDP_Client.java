/**
 * 
 */
package com.example.elhep_mms;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import android.app.Activity;
import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

// This class is currently unused
/**
 * @author Agnieszka Zago�dzi�ska, Andrzej Woje�ski
 *
 */
// Broadcast sending client
// Discovery service
public class UDP_Client extends Activity implements Runnable  {	
	
	public final static String DEBUG_CLIENT = "UDP_CLIENT";
	//public final static String DISCOVERY_PACKET = "ELHEP_MMS:DISCOVERY";
	public final static String DISCOVERY_PACKET = "M-SEARCH * HTTP/1.1\n" +
			"HOST: 239.255.255.250:1900\n" +
			//"HOST: 192.168.0.255:7681\n" +
			"MAN: \"ssdp:discover\"\n" +
			"MX: 5.0\n" +
			"ST:urn:ELHEP:device:MMS:";
			//"ST: ssdp:all";
	
	// Build broadcast address
	// 255.255.255.255 may not work over Wifi
	InetAddress getBroadcastAddress() throws IOException {
		
	    WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
	    DhcpInfo dhcp = wifi.getDhcpInfo();
	    // handle null somehow
	    
	    int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
	    byte[] quads = new byte[4];
	    for (int k = 0; k < 4; k++)
	      quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
	    return InetAddress.getByAddress(quads);
	}
	
	public void run() {	
				
		// Wyslanie danych broadcastowych
		try {			
		
			// On real device need to be changed to getBroadcastAddress
			//InetAddress udp_broadcast = getBroadcastAddress();
			//InetAddress udp_broadcast = InetAddress.getByName("10.0.2.2");
			InetAddress udp_broadcast = InetAddress.getByName("239.255.255.250");
			//InetAddress udp_broadcast = InetAddress.getByName("192.168.0.255");
			// Connect to any interface			
			DatagramSocket socket = new DatagramSocket(MainActivity.CONNECTION_PORT);			
			socket.setBroadcast(true);
			
			byte[] msg = DISCOVERY_PACKET.getBytes();
			DatagramPacket packet = new DatagramPacket(msg, msg.length, udp_broadcast, MainActivity.CONNECTION_PORT);
			socket.send(packet);				
			
			Log.d(DEBUG_CLIENT, "PACKET_SEND");
			
			socket.close();			

		} catch (SocketException e) {
			// TODO Auto-generated catch block
			Log.d(DEBUG_CLIENT, "SOCKET_EXCEPTION");
			//e.printStackTrace();			
			//conn_label.append("SOCKET INIT:" + System.err.toString());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			// InetAddress
			//e.printStackTrace();
			//conn_label.append("INET: " + System.err.toString());
			Log.d(DEBUG_CLIENT, "UNKNOWN_HOST");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			//conn_label.append("SENDING:" + System.err.toString());
			Log.d(DEBUG_CLIENT, "IO_EXCEPTION");
		}
	}

}
