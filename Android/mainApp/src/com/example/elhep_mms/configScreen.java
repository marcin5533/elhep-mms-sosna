package com.example.elhep_mms;

import org.json.JSONException;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class configScreen extends FragmentActivity {

	Device_data deviceData;
	String device_id;
	String device_ver;
	String device_name;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.config_screen);
        
        // copy device data
        deviceData = MainActivity.actualDevice;
        
        // set name of device
        TextView txtDeviceName = (TextView) findViewById(R.id.textView_DeviceName);
        Button btn_sendConfig = (Button) findViewById(R.id.button_send);                
        
        // check if it is bluetooth device (is so, show connect button instead of config)
        // type && json == null
        if (deviceData.type == Device_data.CONNECTION_BLUETOOTH && deviceData.JSON_data == null) {
        	txtDeviceName.setText(deviceData.IP);
        	btn_sendConfig.setText("Connect by Bluetooth");
        }
        else {        
        	// build config interface
        	// scan array uniqueProbes
        	
        	try {
    			device_name = deviceData.JSON_data.getString("deviceName");
    			device_id = deviceData.JSON_data.getString("deviceID");
    			device_ver = deviceData.JSON_data.getString("verID");
    			
    		} catch (JSONException e) {
    			//txtDeviceName.setText("?");
    		}
        	
        	txtDeviceName.setText("Device name: " + device_name + "\nID: " + device_id + " Ver: " + device_ver);
        	
        }
	}
	
	
}
