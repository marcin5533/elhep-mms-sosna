//
//  WUTDiscovery.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
	WUTWiFiDiscovery         = 1 <<  0,
	WUTBluetoothDiscovery    = 1 <<  1,
} WUTDiscoverySource;


@class WUTBLEHandler;

@class WUTSSDPHandler;

@class Device;

@interface WUTDiscovery : NSObject

@property (nonatomic, strong) WUTBLEHandler *bleHandler;

@property (nonatomic, strong) WUTSSDPHandler *ssdpHandler;

@property (nonatomic, readonly) WUTDiscoverySource discoverySource;

- (id)initWithDiscoverySource:(WUTDiscoverySource)source;


- (BOOL)startDiscovery;

- (void)stopDiscovery;


- (void)sendData:(NSData *)data toDevice:(Device *)device;

- (BOOL)startMeasurementsWithDevice:(Device *)device;

@end
