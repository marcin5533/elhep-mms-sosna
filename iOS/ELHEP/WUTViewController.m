//
//  WUTViewController.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTViewController.h"

@interface WUTViewController ()



@end

@implementation WUTViewController
@synthesize isIphone =isIphone_;
- (id)init{
    self = [super initWithNibName:nil bundle:nil];
    if (!self) {
        return nil;
    }
    return self;
}

- (void)unloadView{

}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message{
    
}

- (void)viewDidUnload{
    [super viewDidUnload];
    [self unloadView];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end
