//
//  WUTBLEHandler.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/9/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "WUTBLEHandler.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface WUTBLEHandler () <CBCentralManagerDelegate>{
    CBCentralManager    *centralManager;
	BOOL				pendingInit;
}

@end

@implementation WUTBLEHandler

+ (id) sharedInstance{
	static WUTBLEHandler	*this	= nil;
    
	if (!this)
		this = [[WUTBLEHandler alloc] init];
    
	return this;
}

- (id)init{
    self = [super init];
    
    if(!self)return nil;
    
    pendingInit = YES;
    centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
    
    return self;
}

- (void)startDiscoveryWithServiceUUID:(NSString *)uuidString{
    
    NSArray			*uuidArray	= @[[CBUUID UUIDWithString:uuidString]];
	NSDictionary	*options	= @{CBCentralManagerScanOptionAllowDuplicatesKey : @NO};
    
	[centralManager scanForPeripheralsWithServices:uuidArray options:options];
}

- (void) stopScanning{
	[centralManager stopScan];
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central{
    NSLog(@"BLE did update state");
    static CBCentralManagerState previousState = -1;
    
	switch ([centralManager state]) {
		case CBCentralManagerStatePoweredOff:
		{
            NSLog(@"CBCentralManagerStatePoweredOff");
			break;
		}
            
		case CBCentralManagerStateUnauthorized:
		{
			/* Tell user the app is not allowed. */
            NSLog(@"CBCentralManagerStateUnauthorized");
			break;
		}
            
		case CBCentralManagerStateUnknown:
		{
			/* Bad news, let's wait for another event. */
            NSLog(@"CBCentralManagerStateUnknown");
			break;
		}
            
		case CBCentralManagerStatePoweredOn:
		{
			pendingInit = NO;

			[centralManager retrieveConnectedPeripherals];
            
            NSLog(@"CBCentralManagerStatePoweredOn");

			break;
		}
            
		case CBCentralManagerStateResetting:
		{
			
			pendingInit = YES;
            NSLog(@"CBCentralManagerStateResetting");
            
			break;
		}
        case CBCentralManagerStateUnsupported:{
            NSLog(@"CBCentralManagerStateUnsupported");
            break;
        }
	}
    
    previousState = [centralManager state];
}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals{
    NSLog(@"BLE did retrieve peripheral%@", peripherals);
}

- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals{
    NSLog(@"BLE did retrieve connected peripherals:%@",peripherals);
    
}

- (void)centralManager:(CBCentralManager *)central
 didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary *)advertisementData
                  RSSI:(NSNumber *)RSSI{
    NSLog(@"BLE did discover peripheral");
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral{
    NSLog(@"BLE did connect peripheral");
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    NSLog(@"BLE did fail to connect to peripheral");
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    NSLog(@"BLE did disconnect peripheral");
}

@end
