//
//  ProbeIn.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Probe;

@interface ProbeIn : NSManagedObject

@property (nonatomic, retain) NSNumber * autoTriggerRetry;
@property (nonatomic, retain) NSNumber * numberOfSamples;
@property (nonatomic, retain) NSNumber * sampleFreq;
@property (nonatomic, retain) NSNumber * triggerHigh;
@property (nonatomic, retain) NSNumber * triggerLow;
@property (nonatomic, retain) NSNumber * wasEdited;
@property (nonatomic, retain) Probe *probe;

@end
