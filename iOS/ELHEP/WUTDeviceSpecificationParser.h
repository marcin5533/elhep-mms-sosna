//
//  WUTDeviceSpecificationParser.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface WUTDeviceSpecificationParser : NSObject

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (id)initWithPath:(NSString *)jsonFilePath;

- (id)initWithData:(NSData *)jsonData;

- (BOOL)parseJSONForDeviceWithIPAddress:(NSString *)ipAddress;

- (BOOL)parseJSONForDeviceWithUUID:(NSString *)UUID;

@end
