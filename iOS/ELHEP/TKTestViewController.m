//
//  TKTestViewController.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/30/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "TKTestViewController.h"
#import "WUTPlotView.h"
#import "WUTPlotController.h"
#import "UIView+TKGeometry.h"

@interface TKTestViewController ()

@property (strong, nonatomic) WUTPlotController *controller;

@end

@implementation TKTestViewController

- (id)initWith{
    self = [super initWithNibName:nil bundle:nil];
    if (!self) return nil;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.view.autoresizesSubviews = YES;


    
    WUTPlotView *plot = [[WUTPlotView alloc] initWithFrame:CGRectMake(10.0f, 10.0f,self.view.width - 20.0f, 200.0f)];
    plot.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    
    self.controller = [[WUTPlotController alloc] initWithPlotView:plot];

    [self.view addSubview:plot];
    
    [self.controller startSendingResults];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
