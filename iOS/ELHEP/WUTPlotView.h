//
//  WUTPlotView.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WUTResult;

@protocol WUTPlotViewDataSource;

@interface WUTPlotView : UIView

@property (weak, nonatomic) id<WUTPlotViewDataSource> dataSource;

@property (strong, nonatomic) UIColor *backgroundColor;

@property (strong, nonatomic) UIColor *boundsColor;

@property (strong, nonatomic) UIColor *textColor;

@property (strong, nonatomic) UIColor *lineColor;

@property (strong, nonatomic) UIColor *unitColor;

@property (strong, nonatomic) UIColor *timeColor;

@property (nonatomic, assign) CGFloat unitsMarginWidth;

@property (nonatomic, assign) CGFloat timeMarginWidth;

@property (strong, nonatomic) UIFont *timeFont;

@property (strong, nonatomic) UIFont *unitFont;

@property (nonatomic) CGFloat minValue;

@property (nonatomic) CGFloat maxValue;

@property (nonatomic) NSInteger timeLabelOffset;

@property (nonatomic) CGFloat unitLabelOffset;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

- (id)initWithFrame:(CGRect)frame dataSource:(id<WUTPlotViewDataSource>)dataSource;

@end

@protocol WUTPlotViewDataSource <NSObject>

- (long long)numberOfSamplesForPlotView:(WUTPlotView *)plotView;

- (WUTResult *)plotView:(WUTPlotView *)plotView sampleAtIndex:(long long)index;

@end
