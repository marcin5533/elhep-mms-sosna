//
//  Probe.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Device, ProbeActions, ProbeIn, ProbeOut;

@interface Probe : NSManagedObject

@property (nonatomic, retain) NSNumber * probeID;
@property (nonatomic, retain) NSString * probeName;
@property (nonatomic, retain) Device *device;
@property (nonatomic, retain) ProbeActions *probeActions;
@property (nonatomic, retain) ProbeIn *probeIn;
@property (nonatomic, retain) ProbeOut *probeOut;

@end
