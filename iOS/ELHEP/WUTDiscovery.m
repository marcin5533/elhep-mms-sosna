    //
//  WUTDiscovery.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "WUTDiscovery.h"
#import "WUTSSDPHandler.h"
#import "WUTBLEHandler.h"
#import "WUTDeviceSpecificationParser.h"
#import "TKAppDelegate.h"
#import "Device+Utility.h"
#import "Device+Utility.h"
#import "SRWebSocket.h"
#import "../../Common/mms_configuration.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
// Log levels: off, error, warn, info, verbose
static const int ddLogLevel = LOG_LEVEL_VERBOSE;


@interface WUTDiscovery ()

@property (nonatomic, copy) NSMutableArray *websockets;

@end

@implementation WUTDiscovery

- (id)initWithDiscoverySource:(WUTDiscoverySource)source{
    self = [super init];
    if(!self) return nil;
    
    _discoverySource = source;
    
    if ([self searchWifi]) {
        self.ssdpHandler = [WUTSSDPHandler sharedHandler];
        if (!self.ssdpHandler) {
            [self errorWithMessage:NSLocalizedString(@"Can't create ssdp manager", @"Can't create ssdp manager")];
            return nil;
        }
    }
    
    if ([self searchBluetooth]) {
        self.bleHandler = [[WUTBLEHandler alloc] init];
        [self errorWithMessage:NSLocalizedString(@"Can't create bluetooth manager", @"Message bluetooth manager")];
        if (!self.bleHandler) {
            return nil;
        }
    }
    
    
    return self;
}

- (BOOL)startDiscovery{
    
    if ([self searchBluetooth]) {
        NSString *mmsServiceUUID = @(MMS_BLE_SERVICE_UUID);
        
        [self.bleHandler startDiscoveryWithServiceUUID:mmsServiceUUID];
        
    }
    
    if ([self searchWifi]) {
        if(![self.ssdpHandler startSendingAdvertisements]){
            [self errorWithMessage:NSLocalizedString(@"Can't start sending advertisements", @"Can't send adverts")];
            return NO;
        }
        
        if (![self.ssdpHandler startListeningForAnswers]) {
            [self errorWithMessage:@"Can't start receiving SSDP SEARCH RESPONSE"];
            return NO;
        }
        
        __block typeof(self) bself = self;
        
        [self.ssdpHandler setDidFoundMMSDeviceBlock:^(NSString *ipAddress){
            
            
            if ([bself isDeviceWithAddressAlreadyInDatabase:ipAddress]) {
                return ;
            }
            
            if ([bself isWebSocketForIpAddress:ipAddress]) {
                return;
            }
            
            [bself connectWebSocketToHostWithIPAddress:ipAddress];
            
        }];
        
    }
    
    return YES;
}

- (void)stopDiscovery{
    
}

#pragma mark - Getters

- (NSMutableArray *)websockets{
    if(!_websockets){
        _websockets = [[NSMutableArray alloc] initWithCapacity:0];
    }
    
    return _websockets;
}

#pragma mark - Private

- (void)connectWebSocketToHostWithIPAddress:(NSString*)ipAddress{
    
    NSString *urlString = [NSString stringWithFormat:@"ws://%@:%d",ipAddress, WEB_SOCKET_PORT];
    NSString *wsProtocolName = @(WS_SOCKET_PROTOCOL_NAME);
    
    SRWebSocket *webSocket = [[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]] protocols:@[ wsProtocolName ]];
    webSocket.delegate = (id<SRWebSocketDelegate>)self;
    [webSocket open];
    
    [self addWebSocketManager:webSocket];
}

- (void)errorWithMessage:(NSString *)message{
    
}

- (void)addWebSocketManager:(SRWebSocket *)webSocketManager{
    [self.websockets addObject:webSocketManager];
}

- (void)removeWebSocketManager:(SRWebSocket *)webSocketManager{
    [self.websockets removeObject:webSocketManager];
}

#pragma mark - Helper methods

- (Device *)deviceWithAddress:(NSString *)address{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Device" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(address = %@)", address];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects == nil) {
        return nil;
    }
    
    
    Device *device = [fetchedObjects lastObject];
    
    
    return device;
}

- (BOOL)isDeviceWithAddressAlreadyInDatabase:(NSString *)address{
    
    Device *device  =   [self deviceWithAddress:address];
    
    return (device != nil);
}

- (BOOL)isWebSocketForIpAddress:(NSString *)ipAddress{
    SRWebSocket *ourWebSocket = nil;
    
    DDLogWarn(@"websockets:%@", _websockets);
    
    for (SRWebSocket *websocket in self.websockets) {
        
        if ([ipAddress isEqualToString:websocket.url.host]) {
            ourWebSocket = websocket;
        }
    }
    
    if (ourWebSocket) {
        return YES;
    }
    else{
        return NO;
    }
    
}

- (NSManagedObjectContext *)managedObjectContext{
    
    TKAppDelegate *appDelegate = (TKAppDelegate *)[UIApplication sharedApplication].delegate;
    
    return appDelegate.managedObjectContext;
    
}

- (BOOL)searchWifi{
    if ((self.discoverySource & WUTWiFiDiscovery) == WUTWiFiDiscovery)
        return YES;
    else
        return NO;
}

- (BOOL)searchBluetooth{
    if ((_discoverySource & WUTBluetoothDiscovery) == WUTBluetoothDiscovery)
        return YES;
    else
        return NO;
}

#pragma mark - SRWebSocketDelegate

- (void)webSocketDidOpen:(SRWebSocket *)webSocket{
    DDLogError(@"did open");
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error{
    DDLogError(@"did fail with error %@", [error localizedDescription]);
    [webSocket close];
    [_websockets removeObject:webSocket];
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean{
    DDLogError(@"did close with code %d, reason:%@, was clean:%d", code, reason, wasClean);
    
    [_websockets removeObject:webSocket];
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message{
    
    
    NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
    
    if (!data) {
        [self errorWithMessage:@"Can't decode data from WebSocket"];
        [webSocket close];
        return;
    }
    
    
    
    
    NSString *ipAddress = webSocket.url.host;
    

    if (![self isDeviceWithAddressAlreadyInDatabase:ipAddress]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT , 0), ^{
            WUTDeviceSpecificationParser *parser = [[WUTDeviceSpecificationParser alloc] initWithData:data];
            if (parser) {
                if (![parser parseJSONForDeviceWithIPAddress:ipAddress]) {
                    [self errorWithMessage:@"Can't parse JSON with specification"];
                }
            }
        });
    }else{
        
        NSError * __autoreleasing error = nil;
        
        NSDictionary *dictonaryStatus = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:NSJSONWritingPrettyPrinted
                                                                          error:&error];
        
        Device *device = [self deviceWithAddress:ipAddress];
        
        if (!device.isConnected.boolValue) {
            
            if (!dictonaryStatus) {
                [self errorWithMessage:[NSString stringWithFormat:@"Can't parse JSON with status :%@", [error localizedDescription]]];
                return;
            }
            
            if (dictonaryStatus[@"status"]) {
                BOOL success = ![dictonaryStatus[@"status"] boolValue];
                if (success) {
                    device.isConnected = @(YES);
                    if (![self.managedObjectContext save:&error]) {
                        DDLogError(@"Can't save!");
                        return;
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:kWUTDeviceConnectedNotification
                                                                        object:device
                                                                      userInfo:@{@"socket" : webSocket}];

                }
            }
        }
            
        
    }
}

#pragma mark - Sending Data via WebSocket/Bluetooth

- (void)sendData:(NSData *)data toDevice:(Device *)device{
    
    if (device.interface.intValue != WUTInterfaceWifi) {
        [self errorWithMessage:@"This Device communicates over Bluetooth"];
        return;
    }
    
    SRWebSocket *ourWebSocket = nil;
    
    NSLog(@"websockets:%@", _websockets);
    
    for (SRWebSocket *websocket in self.websockets) {
        
        if ([device.address isEqualToString:websocket.url.host]) {
            ourWebSocket = websocket;
        }
    }
    
    if (!ourWebSocket) {
        [self errorWithMessage:@"Can't find websocket manager"];
        return;
    }
    
    [ourWebSocket send:data];
}

- (BOOL)startMeasurementsWithDevice:(Device *)device{
    NSError * __autoreleasing error = nil;
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:[device configuration] options:NSJSONWritingPrettyPrinted error:&error];
    
    if (!data) {
        [self errorWithMessage:[error localizedDescription]];
        return NO;
    }
    
    [self sendData:data toDevice:device];
    
    return YES;
}

@end
