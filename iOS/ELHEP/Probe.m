//
//  Probe.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "Probe.h"
#import "Device.h"
#import "ProbeActions.h"
#import "ProbeIn.h"
#import "ProbeOut.h"


@implementation Probe

@dynamic probeID;
@dynamic probeName;
@dynamic device;
@dynamic probeActions;
@dynamic probeIn;
@dynamic probeOut;

@end
