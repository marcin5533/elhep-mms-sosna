//
//  ProbeAction.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ProbeActions;

@interface ProbeAction : NSManagedObject

@property (nonatomic, retain) NSString * actionDesc;
@property (nonatomic, retain) NSString * actionName;
@property (nonatomic, retain) NSNumber * actionParameterType;
@property (nonatomic, retain) ProbeActions *probeActions;

@end
