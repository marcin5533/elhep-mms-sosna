//
//  ProbeIn.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "ProbeIn.h"
#import "Probe.h"


@implementation ProbeIn

@dynamic autoTriggerRetry;
@dynamic numberOfSamples;
@dynamic sampleFreq;
@dynamic triggerHigh;
@dynamic triggerLow;
@dynamic wasEdited;
@dynamic probe;

@end
